# NodeJS project for ECE

# START SERVER

### Start server
To launch the server, please input this command into a console open at the root directory of the project.

'''
npm run start
'''
## Docs

### General

* NodeJS : https://nodejs.org/en/
* Npm : https://www.npmjs.com/