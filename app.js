var http = require('http');
var user = require('./src/user.js');

// Declare an http server
http.createServer(function (req, res) {
	if(req.url === '/save') {
		user.save('Charlelie',function(){
			// Write a response header
			res.writeHead(201, {'Content-Type': 'text/plain'});

			// Write a response content
			res.end('Save request : Saving user .\n');	
		});		
	}
	else if(req.url === '/get') {
				
		user.get('12345',function(){
			// Write a response header
			res.writeHead(200, {'Content-Type': 'text/plain'});

			// Write a response content
			res.end('Get request : Pulling user from database');
		});
	}
	else if (req.url ==='/') {
		// Write a response header
		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Write a response content
		res.end('Hello, please save/get a user\n');
	}
	else {
		// Write a response header
		res.writeHead(404, {'Content-Type': 'text/plain'});

		// Write a response content
		res.end('Page not found, please check url');		
	}



// Start the server
}).listen(1337, '127.0.0.1')

// curl localhost:1337 or go to http://localhost:1337