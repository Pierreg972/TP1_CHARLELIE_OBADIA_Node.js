module.exports = {
	save: function (user, callback){
		console.log('User ' + user + ' saved');
		callback();
	},
	get: function (id, callback){
		console.log('User with id : ' + id + ' requested');
		callback();
	}
}